import { Component, OnInit } from '@angular/core';
import { FormArray, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { FamilyService } from '../../services/family.service';

@Component({
  selector: 'lab-js-add-family',
  templateUrl: './add-family.component.html',
  styleUrls: ['./add-family.component.scss']
})
export class AddFamilyComponent implements OnInit {
  private getFamilyMember() {
    return this.fb.group({
      name: ['', Validators.required],
      age: ['', Validators.required]}
    );
  }
  public familyForm: FormGroup = this.fb.group({
    name: ['', Validators.required],
    father: this.getFamilyMember(),
    mother: this.getFamilyMember(),
    children: this.fb.array([
      this.getFamilyMember()
    ])
  });
  public get children() {
    return this.familyForm.get('children') as FormArray;
  }
  public constructor(
    private readonly familyService: FamilyService,
    private readonly fb: FormBuilder,
  ) {}
  public ngOnInit(): void { }
  public addChild() {
    this.children.push(this.getFamilyMember());
  }
  public removeChild(index: number) {
    this.children.removeAt(index);
  }
  public submit() {
    this.familyService.addFamily$(this.familyForm.value).subscribe();
    this.familyForm.reset();
  }
}
